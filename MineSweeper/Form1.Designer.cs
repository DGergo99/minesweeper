﻿
namespace MineSweeper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labelMines = new System.Windows.Forms.Label();
            this.nMinesLabel = new System.Windows.Forms.Label();
            this.secondsGoneLabel = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.restartButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelMines
            // 
            this.labelMines.AutoSize = true;
            this.labelMines.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelMines.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelMines.Location = new System.Drawing.Point(135, 7);
            this.labelMines.Name = "labelMines";
            this.labelMines.Size = new System.Drawing.Size(135, 25);
            this.labelMines.TabIndex = 0;
            this.labelMines.Text = "MINES LEFT:";
            // 
            // nMinesLabel
            // 
            this.nMinesLabel.AutoSize = true;
            this.nMinesLabel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.nMinesLabel.Font = new System.Drawing.Font("MS PGothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nMinesLabel.ForeColor = System.Drawing.Color.Red;
            this.nMinesLabel.Location = new System.Drawing.Point(276, 10);
            this.nMinesLabel.Name = "nMinesLabel";
            this.nMinesLabel.Size = new System.Drawing.Size(20, 20);
            this.nMinesLabel.TabIndex = 1;
            this.nMinesLabel.Text = "0";
            // 
            // secondsGoneLabel
            // 
            this.secondsGoneLabel.AutoSize = true;
            this.secondsGoneLabel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.secondsGoneLabel.Font = new System.Drawing.Font("MS PGothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondsGoneLabel.ForeColor = System.Drawing.Color.Red;
            this.secondsGoneLabel.Location = new System.Drawing.Point(84, 10);
            this.secondsGoneLabel.Name = "secondsGoneLabel";
            this.secondsGoneLabel.Size = new System.Drawing.Size(20, 20);
            this.secondsGoneLabel.TabIndex = 2;
            this.secondsGoneLabel.Text = "0";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelTime.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelTime.Location = new System.Drawing.Point(12, 7);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(66, 25);
            this.labelTime.TabIndex = 3;
            this.labelTime.Text = "TIME:";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // restartButton
            // 
            this.restartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restartButton.Location = new System.Drawing.Point(355, 8);
            this.restartButton.Name = "restartButton";
            this.restartButton.Size = new System.Drawing.Size(116, 23);
            this.restartButton.TabIndex = 4;
            this.restartButton.Text = "RESTART";
            this.restartButton.UseVisualStyleBackColor = true;
            this.restartButton.Click += new System.EventHandler(this.restartButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1039, 553);
            this.Controls.Add(this.restartButton);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.secondsGoneLabel);
            this.Controls.Add(this.nMinesLabel);
            this.Controls.Add(this.labelMines);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "MineSweeper";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMines;
        private System.Windows.Forms.Label nMinesLabel;
        private System.Windows.Forms.Label secondsGoneLabel;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button restartButton;
    }
}

