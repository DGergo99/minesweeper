﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MineSweeper
{
    class ButtonMS : Button
    {
        private bool flagged;
        private bool inactive;

        /// <summary>
        /// Get-Set flagging status of a button.
        /// </summary>
        public bool Flagged
        {
            get { return flagged; }
            set { this.flagged = value; }
        }

        /// <summary>
        /// Get-Set inactivity status of a button.
        /// </summary>
        public bool Inactive
        {
            get { return inactive; }
            set { this.inactive = value; }
        }

        public ButtonMS()
        {
            this.flagged = false;
            this.inactive = false;
        }

        /// <summary>
        /// Button constructor for when the form gets inicialized.
        /// </summary>
        /// <param name="name">Button's name (X index,Y index).</param>
        /// <param name="size">Button's size, normally 25.</param>
        /// <param name="location">Button's location, has to be set for them to appear in rows.</param>
        public ButtonMS(string name, Size size, Point location)
        {
            this.BackColor = default(Color);
            this.Name = name;
            this.Size = size;
            this.Location = location;
            this.flagged = false;
            this.inactive = false;
        }

        /// <summary>
        /// Setting the button's text and color by an integer array representation of rgb.
        /// </summary>
        /// <param name="color">Int array of an rgb color.</param>
        /// <param name="txt">Text to display.</param>
        public void SetTextAndColor(int [] color, string txt)
        {
            this.Text = txt;
            this.BackColor = Color.FromArgb(color[0], color[1], color[2]);
        }

        /// <summary>
        /// Used when we only want to add a color to the button and no text.
        /// </summary>
        /// <param name="c">Color type color.</param>
        public void SetTextAndColor(Color c)
        {
            this.Text = "";
            this.BackColor = c;
        }

        /// <summary>
        /// Used when the restart button is pressed and the buttons get new values. Button set to default.
        /// </summary>
        /// <param name="c">Normally default color.</param>
        /// <param name="btnValue">Activity and flag statuses.</param>
        public void SetTextAndColor(Color c, bool btnValue)
        {
            this.Text = "";
            this.BackColor = c;
            this.BackgroundImage = null;
            this.Inactive = btnValue;
            this.Flagged = btnValue;
        }

        /// <summary>
        /// Used when we have a specific color AND text in mind.
        /// </summary>
        /// <param name="c">Color type color.</param>
        /// <param name="txt">Text to show.</param>
        public void SetTextAndColor(Color c, string txt)
        {
            this.Text = txt;
            this.BackColor = c;
        }

        /// <summary>
        /// Decides between the 3 possible images (mine, flag or none). Sets image width.
        /// </summary>
        /// <param name="imgname">The chosen image.</param>
        public void SetBackgroundImage(string imgname)
        {
            if (imgname.Equals("mine"))
            {
                this.BackgroundImage = Properties.Resources.mine;
            }
            else if (imgname.Equals("flag"))
            {
                this.BackgroundImage = Properties.Resources.flag1;
            }
            else
            {
                this.BackgroundImage = null;
            }

            this.BackgroundImageLayout = ImageLayout.Stretch;
        }
    }
}
