﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace MineSweeper
{
    public partial class Form1 : Form
    {
        public int SizeX = 16;
        public int SizeY = 30;
        public int nOfMines = 99;
        public int clicks;
        public int time;
        public GameArea GA;

        public Form1()
        {
            InitializeComponent();
            CreateGameArea();
        }

        /// <summary>
        /// Creates a new game area to play with. Called when a completely new game is started, or when the game is restarted.
        /// The game variables get their default values.
        /// If the game is started for the first time the buttons get created dynamically.
        /// If the game is restarted the buttons already exist so it changes their values, their statuses and their color.
        /// </summary>
        private void CreateGameArea()
        {
            this.time = 0;
            this.clicks = 0;

            this.GA = new GameArea(this.SizeX, this.SizeY);

            timer1.Enabled = false;

            int LocX = 0;
            int LocY = 0;

            for (int i = 0; i < SizeX; i++)
            {
                for (int j = 0; j < SizeY; j++)
                {
                    GA.setLocMxXY(i, j, "");

                    ButtonMS btn = FindButtonByName(i.ToString() + "," + j.ToString());

                    if (btn == null) {
                        btn = new ButtonMS(i.ToString() + "," + j.ToString(),
                                                new Size(25, 25),
                                                new Point(26 * LocX, (labelMines.Height + 10) + 26 * LocY));

                        btn.MouseDown += new MouseEventHandler(this.btn_Click);
                        Controls.Add(btn);
                    }
                    else
                        btn.SetTextAndColor(default(Color), false);

                    LocX++;
                    
                }
                LocY++;
                LocX = 0;
            }
        }

        /// <summary>
        /// This method is called when something happens to the number of flags, it changes the output of the flags label.
        /// </summary>
        /// <param name="i">Could be a negative or a positive integer, added to the number of available flags.</param>
        private void ChangeNOfMines(int i)
        {
            nOfMines += i;
            nMinesLabel.Text = nOfMines.ToString();
        }

        /// <summary>
        /// This event is called when the user clicks on a button with their mouse.
        /// If it's a right click it means the user is flagging or de-flagging a button.
        /// If it's a left click the user is pressing a button to reveal it's value.
        /// The value could be a number, null or a mine. If it's a mine the game is over, 
        /// else it gets recolored and deactivated, it tries to call <c>Flood</c> on it's neighbors
        /// and checks if there are any unpushed buttons left, if not, the game is over.
        /// </summary>
        private void btn_Click(object sender, EventArgs e)
        {
            MouseEventArgs click = e as MouseEventArgs;

            ButtonMS btn = sender as ButtonMS;

            if(click.Button == MouseButtons.Right && !btn.Flagged && !btn.Inactive && nOfMines > 0)
            {
                btn.SetBackgroundImage("flag");
                btn.SetTextAndColor(Color.Gold);
                ChangeNOfMines(-1);
                btn.Flagged = true;

                return;
            }
            else if(click.Button == MouseButtons.Right && btn.Flagged)
            {
                btn.SetBackgroundImage("");
                btn.SetTextAndColor(default(Color), "");
                ChangeNOfMines(1);
                btn.Flagged = false;

                return;
            }
            else if(click.Button == MouseButtons.Left && !btn.Inactive && !btn.Flagged)
            {
                string[] btnData = btn.Name.Split(',');
                int btnRow = Int32.Parse(btnData[0]);
                int btnCol = Int32.Parse(btnData[1]);

                // if it's the first click a random game area or level needs to be generated
                if (clicks == 0)
                {
                    GA.CreateMx(btnRow, btnCol);
                    ChangeNOfMines(0);
                    timer1.Enabled = true;
                }

                if (GA.getLocMxXY(btnRow, btnCol).Equals("0"))
                {
                    btn.SetTextAndColor(Color.LightGray);
                }
                else if(!GA.getLocMxXY(btnRow, btnCol).Equals("X") && !GA.getLocMxXY(btnRow, btnCol).Equals(""))
                {
                    btn.SetTextAndColor(ChangeColor(GA.getLocMxXY(btnRow, btnCol)), GA.getLocMxXY(btnRow, btnCol));
                }
                else
                {
                    // button is hiding a mine
                    GameOver();
                }
                // calling Flood, if there are empty buttons near, they should automatically appear when this button is clicked
                Flood(btn);
                clicks++;
                btn.Inactive = true;
                // if the number of clicks is equal to the number of non-mine buttons, the game is over
                if (clicks == ((SizeX * SizeY) - nOfMines))
                    GameOver();
            }
        }

        /// <summary>The <c>ChangeColor</c> method is called when the user clicks on a button which is not a mine nor empty.
        /// It returns an rgb color, which is calculated by the number of mines next to the button.</summary>
        /// <param name="mines">The number of mines next to the button.</param>
        /// <returns>Integer array of an rgb color.</returns>
        private int[] ChangeColor(string mines)
        {
            int[] rgb = new int[] { 100, 150, 0 };
            rgb[0] = rgb[0] + (Int32.Parse(mines) * 18);

            return rgb;
        }

        /// <summary>The recursive <c>Flood</c> method is called when the user clicks on an empy space.
        /// If the value is empty, it calls the same function on neighbouring spaces. If it has a number value it
        /// reveals it's value and returns. If it's flagged, inactive or a mine, it just returns.</summary>
        private void Flood(ButtonMS btn)
        {
            if (btn != null)
            {
                string[] btnData = btn.Name.Split(',');
                int btnRow = Int32.Parse(btnData[0]);
                int btnCol = Int32.Parse(btnData[1]);

                if (btn.Inactive || btn.Flagged) return;
                if (GA.getLocMxXY(btnRow, btnCol).Equals("X")) return;

                else if (!GA.getLocMxXY(btnRow, btnCol).Equals("") && !GA.getLocMxXY(btnRow, btnCol).Equals("0"))
                {
                    btn.SetTextAndColor(ChangeColor(GA.getLocMxXY(btnRow, btnCol)), GA.getLocMxXY(btnRow, btnCol));
                    btn.Inactive = true;
                    return;
                }
                else if (GA.getLocMxXY(btnRow, btnCol).Equals("") || GA.getLocMxXY(btnRow, btnCol).Equals("0"))
                {
                    btn.SetTextAndColor(Color.LightGray);
                    btn.Inactive = true;

                    for(int i = btnRow - 1; i < btnRow + 2; i++)
                        for (int j = btnCol - 1; j < btnCol + 2; j++)
                            Flood(FindButtonByName((i).ToString() + "," + (j).ToString()));
                }
            }
        }

        /// <summary>Function <c>GameOver</c> is called when the user clicks on a mine or wins the game.
        /// It reveals the mines and the false flags, deactivates the map.</summary>
        private void GameOver()
        {
            foreach (Control c in this.Controls)
            {
                ButtonMS btn = c as ButtonMS;

                if (btn != null)
                {
                    string[] btnData = btn.Name.Split(',');
                    int btnRow = Int32.Parse(btnData[0]);
                    int btnCol = Int32.Parse(btnData[1]);

                    // not flagged mines
                    if (!btn.Flagged && GA.getLocMxXY(btnRow, btnCol).Equals("X"))
                    {
                        btn.SetBackgroundImage("mine");
                        btn.SetTextAndColor(Color.OrangeRed);
                    }
                    // flagged buttons that are not mines
                    else if(btn.Flagged && !GA.getLocMxXY(btnRow, btnCol).Equals("X"))
                        btn.SetTextAndColor(Color.MediumPurple);

                    btn.Inactive = true;
                }
            }
            timer1.Enabled = false;
        }

        /// <summary>The <c>FindButtonByName</c> method selects a button from the map, by looping through the button names.</summary>
        /// <returns>A ButtonMS object or null.</returns>
        private ButtonMS FindButtonByName(string name)
        {
            foreach (Control c in this.Controls)
            {
                ButtonMS btn = c as ButtonMS;
                if( btn != null)
                {
                    if(btn.Name.Equals(name))
                        return btn;
                }
            }
            return null;
        }

        /// <summary>The timer changes the time label on the form every second.</summary>
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.time < 999)
            {
                time++;
            }
            secondsGoneLabel.Text = time.ToString();
        }

        /// <summary>When the restart button is pressed the whole map gets refreshed.</summary>
        private void restartButton_Click(object sender, EventArgs e)
        {
            CreateGameArea();
            ChangeNOfMines(99 - nOfMines);
        }
    }
}
