﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeper
{
    public class GameArea
    {
        private string[,] locMx;
        private int SizeX = 0;
        private int SizeY = 0;

        public GameArea(int x, int y)
        {
            this.locMx = new string[x, y];
            this.SizeX = x;
            this.SizeY = y;
        }

        public void setLocMxXY(int x, int y, string n)
        {
            this.locMx[x, y] = n;
        }

        public string getLocMxXY(int x, int y)
        {
            return this.locMx[x, y];
        }

        /// <summary>
        /// The game area needs to be created only when the user clicks, so they do not click on a mine on the first click.
        /// </summary>
        /// <param name="FirstClickX">The x coordinate of the first click.</param>
        /// <param name="FirstClickY">The y coordinate of the first click.</param>
        public void CreateMx(int FirstClickX, int FirstClickY)
        {
            PlaceMines(FirstClickX, FirstClickY);
            PlaceNumbers();
        }

        /// <summary>
        /// Checking the number of mines neighboring the current area. Called when creating the map, not while playing.
        /// </summary>
        /// <param name="LocX">The x coordinate of the area.</param>
        /// <param name="LocY">The y coordinate of the area.</param>
        /// <returns></returns>
        private int CheckMinesAround(int LocX, int LocY)
        {
            int mines = 0;

            for (int x = LocX - 1; x < LocX + 2; x++)
            {
                for (int y = LocY - 1; y < LocY + 2; y++)
                {
                     if(InGameArea(x, y))
                        if (getLocMxXY(x, y).Equals("X"))
                            mines++;
                }
            }
            return mines;
        }

        /// <summary>
        /// Calculates the number of neighbouring mines by calling <c>CheckMinesAround</c> for each button.
        /// </summary>
        private void PlaceNumbers()
        {
            for (int x = 0; x < this.SizeX; x++)
            {
                for (int y = 0; y < this.SizeY; y++)
                {
                    if (InGameArea(x, y))
                        if (!getLocMxXY(x, y).Equals("X"))
                            setLocMxXY(x, y, CheckMinesAround(x, y).ToString());
                }
            }
        }

        /// <summary>
        /// Registers the user's first click, when they click on an area the neighbours need to be set to 0, so they have a safe start.
        /// Giving 0 value affirms that no mines is placed next to the first click.
        /// </summary>
        /// <param name="FirstClickX">The x coordinate of the first click.</param>
        /// <param name="FirstClickY">The y coordinate of the first click.</param>
        private void RegisterFirstClick(int FirstClickX, int FirstClickY)
        {
            //Random rnd = new Random();
            //int round = 1;

            for(int x = FirstClickX - 2; x < FirstClickX + 3; x++)
            {
                for (int y = FirstClickY - 2; y < FirstClickY + 3; y++)
                {
                    if(InGameArea(x, y))
                        //if (rnd.Next(100) < round)
                        //if( rnd.NextDouble() > (1 / (1 + Math.Abs(FirstClickX - (FirstClickX - x)))))
                        setLocMxXY(x, y, "0");
                }
            }
        }

        /// <summary>
        /// Checks if some "x" and "y" values are in the limits of the game area, useful when calculating the number of mines for
        /// buttons on the sides of the map.
        /// </summary>
        /// <param name="x">X coordinate of the button.</param>
        /// <param name="y">Y coordinate of the button.</param>
        /// <returns>True or False.</returns>
        private bool InGameArea(int x, int y)
        {
            return x >= 0 && y >= 0 && x < SizeX && y < SizeY;
        }

        /// <summary>
        /// Called when the game area is created, places 99 mines randomly, leaving the area the user clicked on first.
        /// </summary>
        /// <param name="FirstClickX">The x coordinate of the first click.</param>
        /// <param name="FirstClickY">The y coordinate of the first click.</param>
        private void PlaceMines(int FirstClickX, int FirstClickY)
        {
            Random rnd = new Random();
            int nOfMines = 0;

            int randRow;
            int randCol;

            RegisterFirstClick(FirstClickX, FirstClickY);

            while(nOfMines < 99)
            {
                randRow = rnd.Next(this.SizeX);
                randCol = rnd.Next(this.SizeY);
                if(InGameArea(randRow, randCol))
                    if (getLocMxXY(randRow, randCol).Equals(""))
                    {
                        setLocMxXY(randRow, randCol, "X");
                        nOfMines++;
                    }
            }
        }
    }
}
